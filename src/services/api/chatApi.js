export const chatApi = {
    getChats() {
        return this.axios.get('chats').then((res, err) => {
            if (err) {
                return this.catchError(err)
            }
            console.log(res.data.chats, 'res')
            return res.data.chats
        })
    },
    getMessages(chatId) {
        return this.axios.get(`chats/${chatId}/messages`).then((res, err) => {
            if (err) {
                return this.catchError(err)
            }
            this.resHandler(res);
            return res
        })
    },
    editMessage(messageId, text) {
        return this.axios.put(`chats/messages/${messageId}`, {text: text}).then((res, err) => {
            if (err) {
                return this.catchError(err)
            }
            this.resHandler(res);
        })
    },
    deleteMessage(messageId) {
        return this.axios.delete(`chats/messages/${messageId}`).then((res, err) => {
            if (err) {
                return this.catchError(err)
            }
            this.resHandler(res);
        })
    },
    createChat(id) {
        return this.axios.post('chats', {id}).then((res, err) => {
            if (err) {
                return this.catchError(err)
            }
            this.resHandler(res);
            return res
        })
    }
};
