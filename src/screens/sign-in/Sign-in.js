import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Image,
} from 'react-native';
import {Form, Input, Item} from 'native-base';
import axios from 'axios';
let image = require('../../../assets/pexels-photo-561463.jpeg');
import BackendApi from '../../services/api';
import {AuthService} from '../../services/authService';
import {branch} from "baobab-react/higher-order";
import messaging from "@react-native-firebase/messaging";
import {Notifications} from "react-native-notifications";


@branch({
    token: ['token'],
})

export default class SigninScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        username: '',
        password: '',
        token: ''
      },
    };
  }
  componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
      if(this.props.token && JSON.stringify(prevProps.token) !== JSON.stringify(this.props.token) ) {
          this.setState({user: {}});
          this.props.navigation.navigate('DrawerScreen');
      }
  }

    componentDidMount() {
        this.getInitial();
    // axios.get(`http://192.168.0.100:3002/index`)
    //     .then(res => {
    //         console.log('am intrtat')
    //         const nameList = res;
    //         console.log(nameList, 'nameList')
    //     })
  }
    async getInitial() {
        const token = await messaging().getToken();
        console.log(token, 'tokenFCM');
        this.setState({token: token})
    }
  inputValueSet = (text, field) => {
    const user = this.state.user;
    user[field] = text;
    this.setState({
      user,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            width: '80%',
            height: '60%',
            backgroundColor: '#ffffff',
            borderRadius: 40,
          }}>
          <Form>
            <Item
              style={{
                marginTop: 15,
                backgroundColor: '#ffffff28',
                borderRadius: 15,
                width: '80%',
                textAlign: 'center',
                alignSelf: 'center',
              }}>
              <Input
                onChangeText={text => {
                  this.inputValueSet(text, 'username');
                }}
                placeholder="Username"
                placeholderTextColor="gray"
                autoCapitalize="none"
                maxLength={30}
                value={this.state.user.username}
              />
            </Item>
            <Item
              style={{
                marginTop: 15,
                backgroundColor: '#ffffff28',
                borderRadius: 15,
                width: '80%',
                textAlign: 'center',
                alignSelf: 'center',
              }}>
              <Input
                onChangeText={text => {
                  this.inputValueSet(text, 'password');
                }}
                placeholder="Password"
                placeholderTextColor="gray"
                autoCapitalize="none"
                maxLength={30}
                value={this.state.user.password}
                secureTextEntry={true}
              />
            </Item>
          </Form>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
              backgroundColor: 'black',
              borderRadius: 30,
              width: '80%',
              height: '13%',
              justifyContent: 'center',
              marginTop: 15,
            }}
            onPress={() => {
              BackendApi.loginUser({
                email: 'test@gmail.com',
                password: 'Test123!',
                  fcm_token: this.state.token
              }).then(data => {
                this.setState({user: {}});
                this.props.navigation.navigate('DrawerScreen');
              });
            }}>
            <Text style={styles.text}>{this.props.strings.SignIn}</Text>
          </TouchableOpacity>
          <Text style={{marginTop: 15, alignSelf: 'center', color: 'grey'}}>
            OR
          </Text>
          <TouchableOpacity
            onPress={() => {
              BackendApi.loginFacebook();
            }}>
            <Image
              style={{
                alignSelf: 'center',
                width: 300,
                height: 100,
              }}
              source={{
                uri:
                  'https://pngimage.net/wp-content/uploads/2018/06/login-with-facebook-button-png-transparent-7.png',
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              BackendApi.loginGoogle();
            }}>
            <Image
              style={{
                alignSelf: 'center',
                width: 265,
                height: 40,
              }}
              source={{
                uri: 'https://www.addressguru.in/images/googlebutton.png',
              }}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{marginTop: 20}}
            onPress={() => {
              this.props.navigation.navigate('SignUpScreen');
            }}>
            <Text style={{color: 'black'}}>
              Dont have an account?{' '}
              <Text style={{color: 'black', fontWeight: 'bold'}}>Sign Up </Text>
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: 'cover',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#add9e6',
    alignItems: 'center',
  },
  text: {
    color: '#ffffff',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
