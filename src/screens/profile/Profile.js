import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Badge} from 'native-base';
import {AuthService} from '../../services/authService';
import {StateService} from '../../services/state/stateService';
import BackendApi from '../../services/api';
import messaging from '@react-native-firebase/messaging';

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {token: ''};
  }
  componentDidMount(): void {
    this.getInitial();
  }
  async getInitial() {
    const token = await messaging().getToken();
    this.setState({token: token});
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <View style={styles.header}>
            <Text style={styles.headerText}>Profile</Text>
          </View>
        </View>
        <View style={styles.linksBlock}>
          <TouchableOpacity
            style={styles.touchableBlock}
            onPress={() => {
              this.props.navigation.navigate('NotificationsScreen');
            }}>
            <Image
              style={{
                width: 30,
                height: 30,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
                marginBottom: 20,
                backgroundColor: '#3375f5',
              }}
              source={{
                uri:
                  'https://www.materialui.co/materialIcons/social/notifications_white_192x192.png',
              }}
            />
            <Text style={styles.link}>Notifications</Text>
            <Badge danger style={{height: 20}}>
              <Text style={{color: 'white'}}>1</Text>
            </Badge>
          </TouchableOpacity>
          <TouchableOpacity style={styles.touchableBlock}>
            <Image
              style={{
                width: 30,
                height: 30,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
                marginBottom: 20,
                backgroundColor: '#3375f5',
              }}
              source={{
                uri:
                  'https://lh3.googleusercontent.com/proxy/B9HTeRMamavSVU0Y-WguVcinVk4gdmlG1mGuEoRX2AAJXdDwGxrgrAaVEfAN41vCnLK_DywG8RBDcLun41JxjasKOf1uCEE3VC6kqmA6t66KSA',
              }}
            />
            <Text style={styles.link}>Payment History</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.touchableBlock}>
            <Image
              style={{
                width: 30,
                height: 30,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
                marginBottom: 20,
                backgroundColor: '#3375f5',
              }}
              source={{
                uri:
                  'https://www.materialui.co/materialIcons/social/notifications_white_192x192.png',
              }}
            />
            <Text style={styles.link}>Dashboard</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.touchableBlock}>
            <Image
              style={{
                width: 30,
                height: 30,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
                marginBottom: 20,
                backgroundColor: '#3375f5',
              }}
              source={{
                uri:
                  'https://www.materialui.co/materialIcons/social/notifications_white_192x192.png',
              }}
            />
            <Text style={styles.link}>Edit</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchableBlock}
            onPress={() => {
              AuthService.logout();
              StateService.setField('token', null);
              BackendApi.logout(this.state.token);
              this.props.navigation.navigate('Login');
            }}>
            <Image
              style={{
                width: 30,
                height: 30,
                borderRadius: 75,
                borderWidth: 3,
                marginRight: 5,
                marginBottom: 20,
                backgroundColor: '#3375f5',
              }}
              source={{
                uri:
                  'https://pngimage.net/wp-content/uploads/2018/06/logout-icon-png-white-2.png',
              }}
            />
            <Text style={styles.link}>Logout</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.whiteBlock}>
          <Image
            style={{
              width: 75,
              height: 75,
              borderRadius: 75,
              borderWidth: 3,
              marginRight: 5,
              marginBottom: 20,
            }}
            source={{
              uri:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRx3F56cenkoLB7t1SgI20Pze1-fzVB5Q5DKbVmvGHCzmvknrRZ&usqp=CAU',
            }}
          />
          <Text style={styles.userName}> Radik Apachita</Text>
          <Text style={styles.email}> apachita22@gmail.com</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
  },
  header: {
    backgroundColor: '#3375f5',
    height: 220,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  headerText: {
    color: 'white',
    fontSize: 21,
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: 40,
  },
  whiteBlock: {
    backgroundColor: 'white',
    position: 'absolute',
    top: 100,
    borderRadius: 15,
    width: '90%',
    height: 220,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  userName: {
    fontSize: 19,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  email: {
    color: '#d4d4d4',
    fontSize: 16,
    fontWeight: 'bold',
  },
  linksBlock: {
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginBottom: 25,
    marginLeft: 50,
  },
  link: {
    fontSize: 18,
    marginBottom: 55,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    marginLeft: 30,
    marginRight: 10,
  },
  touchableBlock: {
    display: 'flex',
    flexDirection: 'row',
  },
});
