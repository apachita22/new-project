/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Linking,
} from 'react-native';
import {root} from 'baobab-react/higher-order';
import {state} from '../src/services/state/State';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AppNavigation from './navigation/app.navigation';
import {Root} from 'native-base';
import {AuthService} from './services/authService';
import {StateService} from './services/state/stateService';
import BackendApi from './services/api';
import messaging from '@react-native-firebase/messaging';

// const app: () => React$Node = () => {
//     return (
//         <AppNavigation/>
//     );
// };
Linking.addEventListener('url', e => {
  AuthService.setToken(e.url.split('token/')[1]);
  StateService.setField('token', e.url.split('token/')[1]);
  BackendApi.setToken(e.url.split('token/')[1]);
});

console.disableYellowBox = true;
class AppComponent extends Component {
  render() {
    return (
      <Root>
        <AppNavigation />
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
});

export const App = root(state, AppComponent);
